#!/bin/bash

# Function to print a success message in green text
print_success() {
    echo -e "\e[32mCommand \"$1\" executed successfully\e[0m"
}

# Function to send device specifications to API
send_device_specs_to_api() {
    echo "Sending device specifications to API..."
    CPU_INFO=$(lscpu)
    MEMORY_INFO=$(free -m)
    STORAGE_INFO=$(df -h)
    PUBLIC_IP=$(curl -s ifconfig.me)

    # Convert the info to JSON format. You might need to adjust this depending on how your API expects to receive the data.
    JSON_PAYLOAD=$(printf '{"cpu_info": "%s", "memory_info": "%s", "storage_info": "%s", "public_ip": "%s"}' "$CPU_INFO" "$MEMORY_INFO" "$STORAGE_INFO" "$PUBLIC_IP")

    # Send the JSON payload to your API. Replace 'http://your-api-url.com' with your actual API URL.
    curl -X POST -H "Content-Type: application/json" -d "$JSON_PAYLOAD" http://your-api-url.com
}

# Function to display a loading animation
display_loading() {
    local pid=$1
    local delay=0.75
    local spinstr='|/-\'
    while [ "$(ps a | awk '{print $1}' | grep $pid)" ]; do
        local temp=${spinstr#?}
        printf " [%c]  " "$spinstr"
        local spinstr=$temp${spinstr%"$temp"}
        sleep $delay
        printf "\b\b\b\b\b\b"
    done
    printf "    \b\b\b\b"
}

# Function to install panel
install_panel() {
    echo "Installing panel..."
    (
    sudo apt-get install apache2 curl subversion php8.1 php8.1-gd php8.1-zip libapache2-mod-php8.1 php8.1-curl php8.1-mysql php8.1-xmlrpc php-pear mariadb-server-10.6 php8.1-mbstring git php-bcmath
    sudo sed -i "s/^bind-address.*/bind-address=0.0.0.0/g" "/etc/mysql/mariadb.conf.d/50-server.cnf"
    sudo mysql_secure_installation
    sudo apt-get install phpmyadmin
    wget -N "https://github.com/OpenGamePanel/Easy-Installers/raw/master/Linux/Debian-Ubuntu/ogp-panel-latest.deb" -O "ogp-panel-latest.deb"
    sudo dpkg -i "ogp-panel-latest.deb"
    ) &
    # Get the PID of the process running in the background
    local pid=$!
    # Display the loading animation until the background process finishes
    display_loading $pid
    echo "Panel installed. Please open a browser and go to http://{IP_OF_SERVER_OR_localhost}/index.php to complete the OGP Panel installation."
}

# Function to install agent
install_agent() {
    echo "Installing agent..."
    (
    sudo apt-get update
    sudo apt-get upgrade
    sudo apt-get install libxml-parser-perl libpath-class-perl perl-modules screen rsync sudo e2fsprogs unzip subversion libarchive-extract-perl pure-ftpd libarchive-zip-perl libc6 libgcc1 git curl
    sudo apt-get install libc6-i386
    sudo apt-get install libgcc1:i386
    sudo apt-get install lib32gcc1
    sudo apt-get install lib32gcc-s1
    sudo apt-get install libhttp-daemon-perl
    wget -N "https://github.com/OpenGamePanel/Easy-Installers/raw/master/Linux/Debian-Ubuntu/ogp-agent-latest.deb" -O "ogp-agent-latest.deb"
    sudo dpkg -i "ogp-agent-latest.deb"
    ) &
    # Get the PID of the process running in the background
    local pid=$!
    # Display the loading animation until the background process finishes
    display_loading $pid
    echo "Agent installed. You can view the automatically generated encryption key, OGP username, and OGP user password by running 'sudo cat /root/ogp_user_password'."
}

# Function to check database password
check_db_password() {
    echo "Checking database password..."
    if [ -f /root/ogp_panel_mysql_info ]; then
        sudo cat /root/ogp_panel_mysql_info
    else
        echo -e "\e[31mError: File does not exist. Please install the panel first.\e[0m"
    fi
}

# Function to check encryption key
check_encryption_key() {
    echo "Checking encryption key..."
    if [ -f /root/ogp_user_password ]; then
        sudo cat /root/ogp_user_password
    else
        echo -e "\e31mError: File does not exist. Please install the agent first.\e[0m"
    fi
}

# Function to check device specifications
check_device_specs() {
    echo "Checking device specifications..."
    echo -e "\e[96mCPU Information:\e[0m"
    lscpu
    echo -e "\e[96mMemory Information:\e[0m"
    free -m
    echo -e "\e[96mStorage Information:\e[0m"
    df -h
}

# Function to show credits
show_credits() {
    echo "Credits: Your Name"
    # Add more credits if needed
}

# Main menu
while true; do
    clear
    echo -e "\n\e[1;33m===================================="
    echo -e "           MAIN MENU"
    echo -e "====================================\e[0m"
    echo -e "1) \e[96mInstall Panel\e[0m"
    echo -e "2) \e[96mInstall Agent\e[0m"
    echo -e "3) \e[96mCheck Database Password\e[0m"
    echo -e "4) \e[96mCheck Encryption Key\e[0m"
    echo -e "5) \e[96mCheck Device Specifications\e[0m"
    echo -e "6) \e[96mShow Credits\e[0m"
    echo -e "7) \e[91mExit\e[0m"
    echo -e "\e[1;33m====================================\e[0m"
    read -p "Enter your choice (1-7): " choice

    case $choice in
        1)
            install_panel
            ;;
        2)
            install_agent
            ;;
        3)
            check_db_password
            ;;
        4)
            check_encryption_key
            ;;
        5)
            check_device_specs
            ;;
        6)
            show_credits
            ;;
        7)
            echo -e "\e[1;31mExiting...\e[0m"
            exit 0
            ;;
        *)
            echo -e "\e[1;31mInvalid choice. Please enter a number between 1 and 7.\e[0m"
            ;;
    esac
    echo -e "\nPress any key to continue..."
    read -n 1
done
